<?php

class Mgcs_KeepCoupon_Model_Observer
{

    /**
     * Save the coupon code before collectTotals() is able to remove it
     *
     * @event sales_quote_collect_totals_before
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function saveCoupon(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote */
        $quote = $observer->getEvent()->getQuote();
        if ($quote->getCouponCode() > '') {
            $quote->setKeepcouponCode($quote->getCouponCode());
        } elseif ($quote->getOrigCouponCode() > '') {
            // after the quote was loaded, there was a coupon code
            // now it has disappeared. apparently someone wants to
            // clear the coupon code
            $quote->setKeepcouponCode('');
        } elseif ($quote->getKeepcouponCode() > '') {
            $quote->setCouponCode($quote->getKeepcouponCode());
        }
    }

    /**
     * Set the original coupon code after loading the quote object
     *
     * Unfortunately, because the quote gets loaded by loadByCustomer(),
     * setOrigData() is not called so there is no original data available.
     * To get around this, we'll set a custom variable on the quote.
     *
     * @event sales_quote_load_after
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function setOriginalCouponCode(Varien_Event_Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $quote->setOrigCouponCode($quote->getCouponCode());
    }


    /**
     * Copy a remembered coupon code to the destination quote after merging
     *
     * This is not necessary if the source quote had a regular coupon code set; it gets copied by
     * Mage_Sales_Model_Quote::merge().
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function quoteMerge(Varien_Event_Observer $observer)
    {
        $destinationQuote = $observer->getEvent()->getQuote();
        $sourceQuote = $observer->getEvent()->getSource();

        if (($code = $sourceQuote->getKeepcouponCode()) && !$sourceQuote->getCouponCode()) {
            if (count($destinationQuote->getAllItems()) > 0) {
                $destinationQuote->setCouponCode($code);
            } else {
                $destinationQuote->setKeepcouponCode($code);
            }
        }
    }


    /**
     * Remove a remembered coupon prior to removing the coupon code
     *
     * @param Varien_Event_Observer $observer
     * @return void
     */
    public function removeCoupon(Varien_Event_Observer $observer)
    {
        $cart = Mage::getSingleton('checkout/cart');
        $quote = $cart->getQuote();
        $quote->setKeepcouponCode(null)
            ->save();
        return;
    }

}