Mgcs_KeepCoupon
===============

Keep a coupon code, even if the quote's grand total is 0.

Facts
-----

* Version 1.0.0
* Module name: Mgcs_KeepCoupon
* Author: Mark van der Sanden / Magentics
* Magento Connect 1.0 extension key: n/a
* Magento Connect 2.0 extension key: n/a

Description
-----------

By default, Magento clears a coupon code if it gets applied to an empty quote (zero grand total). In some situations, this is unwanted. For example, if you want a customer be able to enter the coupon code upfront. You then want the coupon code to be applied at the moment that a product gets added to the cart.

Compatibility
-------------

* Magento CE 1.6+
* Magento EE 1.11+

Installation
------------

* Use [Modman](https://github.com/colinmollenhour/modman)

Uninstallation
--------------

1. Remove all extension files from your Magento installation

Support
-------

If you have any issues with this extension, open an issue on [Bitbucket](https://bitbucket.org/magentics/mgcs_keepcoupon)

Contribution
------------

Any contribution is highly appreciated. The best way to contribute code is to open a [pull request on Bitbucket](https://www.atlassian.com/git/tutorials/making-a-pull-request).

Lead Developers
---------------

Mark van der Sanden http://www.magentics.nl/ [@magentics_nl](https://twitter.com/magentics_nl)

License
-------

[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)
